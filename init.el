(when (version< emacs-version "27")
  (let ((early-init (expand-file-name "early-init.el" user-emacs-directory)))
    (load early-init)
    (package-initialize)))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Corfu, Flymake, et cetera
(use-package corfu
  :ensure t
  :commands global-corfu-mode
  :custom
  (corfu-cycle t)
  (corfu-auto nil)
  :bind (([backtab] . completion-at-point))
  :init
  (global-corfu-mode))

(use-package flymake
  :init
  (add-hook 'prog-mode-hook #'flymake-mode))

(use-package magit
  :ensure t
  :ensure forge)

(use-package yasnippet
  :ensure t
  :commands yas-global-mode
  :config
  (yas-global-mode t))

(defun my/is-theme-usable ()
    (or (display-graphic-p)
	(string-match ".+-256color$" (tty-type))))
(defun my/set-theme ()
  (when (my/is-theme-usable)
    (load-theme 'tango t nil)))
(if (daemonp)
    (add-hook 'after-make-frame-functions
	      (defun my/theme-init-daemon (frame)
		(with-selected-frame frame
		  (my/set-theme))
		(remove-hook 'after-make-frame-functions
			     #'my/theme-init-daemon)
		(fmakunbound 'my/theme-init-daemon)))
  (my/set-theme))

;; eglot for LSP support
(defun my/eglot-setup (_)
  (eldoc-mode 1))
(use-package eglot
  :ensure t
  :commands eglot
  :custom
  (eglot-extend-to-xref t "Make sure we can actually follow things even in xref")
  :config
  ;; JDTLS is actually stupid...
  (cl-defmethod eglot-execute-command (_server (_cmd (eql java.apply.workspaceEdit)) arguments)
    "Eclipse JDT breaks spec and replies with edits as arguments..."
    (mapc #'eglot--apply-workspace-edit arguments))
  (add-to-list 'eglot-server-initialized-hook
	       #'my/eglot-setup))

;; Project configuration
(use-package project
  :custom
  (project-compilation-buffer-name-function #'project-prefixed-buffer-name))

;; C and C++ stuff
(use-package meson-mode
  :ensure t)

;; Javascript & Typescript
(use-package typescript-mode
  :ensure t)
(with-eval-after-load "eglot"
  (add-to-list 'eglot-server-programs
	       '(typescript-mode . ("typescript-language-server" "--stdio"))))

;; Python
(use-package python
  :ensure anaconda-mode
  :hook ((python-mode . anaconda-mode)
	 (python-mode . anaconda-eldoc-mode)))

;; Haskell
(use-package haskell-mode
  :ensure t)

;; Documentation
(use-package markdown-mode
  :ensure t
  :ensure edit-indirect)
(use-package adoc-mode
  :ensure t)

;; Pinetry via emacs
(use-package pinentry
  :ensure t
  :if (executable-find "pinentry-emacs")
  :init
  (pinentry-start))

;; Password store
(use-package pass
  :ensure t)

;; NASM-mode for x86 and AMD64 assembler fun!
(use-package nasm-mode
  :ensure t
  :mode ("\\.nasm$" . nasm-mode))

;; Erlang/Elixir
(use-package erlang
  :ensure t
  :bind*
  ([remap erlang-complete-tag] . completion-at-point))

(use-package elixir-mode
  :ensure t)

;; AUCTeX for doing LaTeX stuff
(use-package tex
  :ensure auctex
  :if (executable-find "lualatex")
  :config
  (add-to-list 'TeX-command-list
	       '("Latex Make" "latexmk %(PDF) %t" TeX-run-TeX))
  :custom
  (TeX-engine 'luatex)
  (TeX-view-program-selection '((output-pdf "Zathura")))
  (preview-image-type 'dvipng))

;; Ibuffer
(defun my/ibuffer ()
  (interactive)
  (ibuffer t))
(use-package ibuffer
  :bind*
  ([remap list-buffers] . #'my/ibuffer))

;;; Common Lisp
;; Load SLIME if available from Quicklisp & set up everything else
;; Use Roswell if available
(use-package slime
  :ensure t
  :init
  (slime-setup '(slime-fancy slime-asdf slime-tramp))
  :commands slime-setup)
(let ((slime-helper (expand-file-name "~/quicklisp/slime-helper.el"))
      (roswell-helper (expand-file-name "~/.roswell/helper.el")))
  (cond
   ((file-exists-p roswell-helper)
    (load roswell-helper)
    (customize-set-variable 'inferior-lisp-program "ros -Q run"))
   ((file-exists-p slime-helper)
    (load slime-helper)
    (customize-set-variable 'inferior-lisp-program "sbcl"))))

;; Scheme
(use-package scheme
  :mode ("\\.sld\\'" . scheme-mode))

(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package geiser
  :ensure t
  :ensure geiser-guile
  :ensure geiser-gauche
  :ensure geiser-chicken
  :custom
  (geiser-active-implementations '(guile gauche chicken))
  (geiser-repl-per-project-p t))

;; Toolbox
(defvar my/toolbox-command "toolbox"
  "Specify the command used to interact with Toolbox.

For Fedora Silverblue, a useful value might be \"flatpak-spawn --host toolbox\"")
(defun my/toolbox-tramp-completion (&optional ignored)
  "Return list of tuples of form (NIL name).

IGNORED is ignored since the toolboxes are identified with the associated command MY/TOOLBOX-COMMAND."
  (let ((containers (cdr (process-lines "/bin/sh" "-c"
					(string-join (list my/toolbox-command
							   "list"
							   "--containers") " ")))))
    (cl-loop for container in containers
	     collect (let ((name (cl-second (split-string container "[[:space:]]+"))))
		       (list nil name)))))

(add-hook 'after-init-hook
	  (lambda ()
	    (push
	     (cons
	      "toolbox"
	      `((tramp-login-program ,my/toolbox-command)
		(tramp-login-args (nil ("run") ("-c") ("%h") ("sh")))
		(tramp-remote-shell "/bin/sh")
		(tramp-remote-shell-args ("-i") ("-c"))))
	     tramp-methods)
	    (tramp-set-completion-function "toolbox" `((,#'my/toolbox-tramp-completion "")))))

(use-package dired
  :custom
  (dired-dwim-target t))

;; Calendar
(use-package calendar
  :custom
  ;; Weeks start from Monday, damn it!
  (calendar-week-start-day 1))

(tool-bar-mode -1)
(menu-bar-mode -1)
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))
(column-number-mode 1)
(setq disabled-command-function nil)

;; Compilation
(ignore-errors
  (require 'ansi-color)
  (defun my-colorize-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'my-colorize-compilation-buffer))

;; Configure icomplete & other completions
(use-package orderless
  :ensure t)
(icomplete-mode 1)
(setq icomplete-show-matches-on-no-input t
      icomplete-hide-common-prefix nil
      icomplete-prospects-height 5
      completion-styles '(orderless basic partial-completion substring emacs22)
      completion-category-overrides '((file (styles basic partial-completion substring emacs22))))

;; Configure mu4e with a local setup, if found.
(let ((mu4e-conf (expand-file-name "mu4econf.el" user-emacs-directory))
      (mu4e-found (require 'mu4e nil t)))
  (when (and (file-exists-p mu4e-conf) mu4e-found)
    (load mu4e-conf)))

;; Enable electic-pair-mode by default
(add-hook 'prog-mode-hook
	  (apply-partially #'electric-pair-mode 1))

;; Enable show-parens-mode
(add-hook 'prog-mode-hook
	  (apply-partially #'show-paren-mode 1))
(customize-set-variable 'show-paren-style 'mixed)

;; Try to find and set a font if one exists
(defun my/set-font ()
  (cond
   ((find-font (font-spec :family "Iosevka"))
    (set-face-attribute 'default nil
			:family "Iosevka"
			:weight 'normal))
   ((find-font (font-spec :family "IBM Plex Mono"))
    (set-face-attribute 'default nil
			:family "IBM Plex Mono"
			:weight 'normal))))
(add-hook 'after-make-frame-functions
	  (defun my/set-new-frame-font (frame)
	    (with-selected-frame frame
	      (my/set-font))))
(my/set-font)

;; Make sure a newline is always appended
(customize-set-variable 'require-final-newline 'visit-save)

(load custom-file)
